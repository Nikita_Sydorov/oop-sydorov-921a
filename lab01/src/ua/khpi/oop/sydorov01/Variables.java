/**
 * 
 */
package ua.khpi.oop.sydorov01;

/**
 * @author Nikita Sydorov
 *
 */
public class Variables {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		/* створення змінних */
		long record_book_number = 0x8770;
		long phone_number = 380668465233L;
		int last_two_phone_number = 0b100001;
		int last_four_phone_number = 012161;
		float number_on_the_list = (((15-1)%26)+1);
		char symbol = 'O';
		int symbol_int = (int) symbol;
		
		/* створення масиву для зберігання цифр та роботи з ними */
		int i = 0;
		long[] array = new long[27];
		
		/* розкладання на цифри phone_number */
		for(int j = 0; j < 13; j++) {
			if(phone_number > 0) {		//380668465233
				array[i] = phone_number % 10;
				phone_number = phone_number / 10;
				i++;
			}
		}
		
		/* розкладання на цифри record_book_number */
		for(int j = 0; j < 13; j++) {
			if(record_book_number > 0) {		//34672
				array[i] = record_book_number % 10;
				record_book_number = record_book_number / 10;
				i++;
			}
		}
		
		/* розкладання на цифри last_two_phone_number */
		for(int j = 0; j < 13; j++) {
			if(last_two_phone_number > 0) {		//33
				array[i] = last_two_phone_number % 10;
				last_two_phone_number = last_two_phone_number / 10;
				i++;
			}
		}
		
		/* розкладання на цифри last_four_phone_number */
		for(int j = 0; j < 13; j++) {
			if(last_four_phone_number > 0) {		//5233
				array[i] = last_four_phone_number % 10;
				last_four_phone_number = last_four_phone_number / 10;
				i++;
			}
		}
		
		/* розкладання на цифри number_on_the_list */
		int number_on_the_list_int = (int) number_on_the_list;
		for(int j = 0; j < 13; j++) {
			if(number_on_the_list_int > 0) {		//15
				array[i] = number_on_the_list_int % 10;
				number_on_the_list_int = number_on_the_list_int / 10;
				i++;
			}
		}
		
		/* розкладання на цифри symbol_int */
		for(int j = 0; j < 13; j++) {
			if(symbol_int > 0) {		//79
				array[i] = symbol_int % 10;
				symbol_int = symbol_int / 10;
				i++;
			}
		}
		
		/* розрахунок кількості парних і непарних цифр в змінних у десятковому записі */
		int par_num = 0;
		int nepar_num = 0;
		for(int j = 0; j < 27; j++) {
			if(array[j] % 2 == 0) {
				par_num++;
			}else {
				nepar_num++;
			}
		}
		
		/* розрахунок кількості одиниць та нулів у змінних в двійковому записі */
		int one = 0;
		int nul = 0;
		
		/* масив для зберігання чисел у двійковому записі
		 * один біт - один єлемент
		 */
		int c = 0;
		long[] bit_array = new long[69];
		for(int j = 0; j < 27; j++) {
			long num = array [j];
			int num_int = (int) num;
			while(num_int != 0) {
				bit_array[c] = num_int%2;
				num_int = num_int / 2;
				c++;
			}
			if(array[j] == 0) {
				bit_array[c] = 0;
				c++;
			}
		}
		
		/* розрахунок одиниць та нулів */
		for(int j = 0; j < 69; j++) {
			if(bit_array[j] == 1) {
				one++;
			}else {
				nul++;
			}
		}	
	}
}
