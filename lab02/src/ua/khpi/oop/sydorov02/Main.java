package ua.khpi.oop.sydorov02;

import java.util.Random;

/**
 * @author Nikita Sydorov
 *
 */
public class Main {

	/**
	 * @param num number check
	 */
	
	public static int simpl_num(int num) {
		int ans = 0;
		if(num < 2) {
			return ans;
		}
		double s = Math.sqrt(num);
		for(int i = 2; i <= s; i++) {
			if(num % i == 0) {
				return ans;
			}
			ans = 1;
		}
		return ans;
	}
	
	/**
	 * @param num counting the number of indents
	 */
	public static int kol_space(int num) {
		int i = 0;
		while(num != 0) {
			num = num / 10;
			i++;
		}
		int kol = 10 - i;
		return kol;
	}
	
	public static void main(String[] args) {
		
		int num;
		System.out.println("|----numb----|--ans--|");
		for(int i = 0; i < 11; i++) {
			final Random random = new Random();
			num = random.nextInt(2_147_483_647);
			int ans = simpl_num(num);
			int kol = kol_space(num);
			System.out.print("| " + num);
			while(kol != 0) { 	// вирівнювання таблиці шляхом додавання пропусків
				System.out.print(" ");
				kol--;
			}
			System.out.println(" |   " + ans + "   |");
		}
		System.out.println("|--------------------|");
		
	}
}
